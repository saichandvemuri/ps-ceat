import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
selector: 'app-login',
templateUrl: './login.component.html',
styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
//global variables
public loginForm: FormGroup;
public formError: boolean = false;
public ShowPassword: boolean = false;
public loginFailed: boolean = false;
public responsedata: any;


constructor(private fb: FormBuilder, private router: Router) { }

ngOnInit(): void {
console.log("working");

this.loginForm = this.fb.group({
email: new FormControl(null, [Validators.required]),
password: new FormControl(null, [Validators.required])
})

}
// to validate form inputs
get form() {
return this.loginForm.controls;
}
// method used for login and call API
public login() {
this.formError = true;
if (this.loginForm.valid) {
this.router.navigateByUrl('list')

}
}

//method to show password

public showpassword() {
this.ShowPassword = !this.ShowPassword;
}


}